/* 

Main Predicate - deduce/3
deduce(+ListofPeople, ?Statements, -Verdict) 
   
*/
deduce(People, Statements, Verdict) :-
    peoplemaker(People, Verdict),
    cases(Verdict, Statements, Cases),
    check(Verdict, Cases).

/* 
    
    To instantiate list of verdict containing the the roles of inhabitants
   
*/

%Base case
peoplemaker([],[]).

peoplemaker([P|Ps], [R|Rs]):- 
    R = role(P, Z),
    Roles = [incomplete, knight, knave],
    member(Z, Roles),
    peoplemaker(Ps, Rs).

/* 

    Cases --------------------------------------------------------------
    All the possible cases given known information

*/

% Base case
cases(_,[],[]).

/* 
    Statement handling for multiple says
*/
% X knight, statement always true
cases(Verdict, [says(X, says(Y, Statement))|Ss], Cases) :-
    member(role(X, knight), Verdict),
    cases(Verdict, [says(Y, Statement)|Ss], Cases).

% X knave, statement doesn't matter
cases(Verdict, [says(X, says(_, _))|Ss], Cases) :-
    member(role(X, knave), Verdict),
    cases(Verdict, Ss, Cases).

/* 
    Regular cases
*/
% X knight says Y knight => Y knight
cases(Verdict, [says(X, role(Y, knight))|Ss], [C|Cs]) :-
    member(role(X, knight), Verdict),
    C = role(Y, knight),
    cases(Verdict, Ss, Cs). 

% X knight says Y knave => Y knave
cases(Verdict, [says(X, role(Y, knave))|Ss], [C|Cs]) :-
    member(role(X, knight), Verdict),
    C = role(Y, knave),
    cases(Verdict, Ss, Cs). 

% X knave says Y knight => Y knave
cases(Verdict, [says(X, role(Y, knight))|Ss], [C|Cs]) :-
    member(role(X, knave), Verdict),
    C = role(Y, knave),
    cases(Verdict, Ss, Cs). 

% X knave says Y knave => Y knight
cases(Verdict, [says(X, role(Y, knave))|Ss], [C|Cs]) :-
    member(role(X, knave), Verdict),
    C = role(Y, knight),
    cases(Verdict, Ss, Cs). 

/* 

    To ensure no contradicting facts 

*/
check([], _).

check([role(X, knight)|Verds], Cases) :-
    \+ (member(role(X, knave), Cases)),
    check(Verds, Cases).

check([role(X, knave)|Verds], Cases) :-
    \+ (member(role(X, knight), Cases)),
    check(Verds, Cases).