**Kelompok** Epilog | **Anggota** Andika Hanavian Atmam (1606918585), Felicia (1506726183)

Instruction
---

`swipl program.pl`  

 Main predicate is deduce/3  
 Statements are composed by role/2 and says/2  

> ### **deduce**(+ListofPeople, ?Statements, -Verdict) 
 True if *Verdict* contains all the possible roles within the provided *ListofPeople* and *Statements*. Fails if *ListofPeople* and *Statements* do not match or no possible roles are found.

> ### **role**(+PeopleName, +Role) 

Denotes *PeopleName* is of type of *Role*


> ### **says**(+PeopleName, ?Statement) 

Denotes *PeopleName* says *Statement*. *Statement* Could be of role/2 or says/2.

## Example Input  
[Based on Wikipedia](https://en.wikipedia.org/wiki/Knights_and_Knaves) description of the original Raymond Smullyan's Knight and Knave puzzle  
> One of Smullyan's examples of this type of puzzle involves three inhabitants referred to as A, B and C. The visitor asks A what type he is, but does not hear A's answer. B then says "A said that he is a knave" and C says "Don't believe B; he is lying!"

Three inhabitants are referred  
- A
- B  
- C

Two distinct statements can be infered:  
- B said A said A is a knave
- C said B is a knave

Hence  
`-? deduce([a,b,c], [says(b, says(a, role(a, knave))), says(c, role(b, knave))], Verdict).
`

Project is deemed complete and within the scope of the proposal.  
---

NICE FEATURE TODO  
[x] Ability to proceed unlimited amount of people  
[] Ability to handle Spy/Normal  
[] Generalize to truth-tellers and liars https://brilliant.org/wiki/truth-tellers-and-liars/  
[] Ability to handle https://philosophy.hku.hk/think/logic/knights.php   

